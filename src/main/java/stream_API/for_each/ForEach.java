package stream_API.for_each;

import java.util.Arrays;
import java.util.List;

public class ForEach {
    //        Na potrzeby zadania skorzystaj z następującej listy:
    //        Arrays.asList(“Piotr”, “Joanna”, “Krzysztof”);
    //        1. Napisz funkcję, która wyświetli wszystkie elementy listy
    //        2. Napisz funkcję, która wyświetli długość każdego elementu listy
    //        3. Napisz funkcję, która wyświetli czy długość tekstu dla każdego jest parzysta bądź nie.
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Piotr", "Joanna", "Krzysztof");
        namesPrinter(names);
        namesLengthPrinter(names);
        isNamesLengthEvenPrinter(names);
    }

    public static void namesPrinter(List<String> names) {
        names.stream().
                forEach((String name) -> System.out.println(name));
    }

    public static void namesLengthPrinter(List<String> names) {
        names.stream().
                forEach((String name) -> System.out.println(name.length()));
    }

    public static void isNamesLengthEvenPrinter(List<String> names) {
        names.stream().
                forEach((String name) -> System.out.println(name.length() % 2 == 0));
    }
}
