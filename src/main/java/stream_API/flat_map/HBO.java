package stream_API.flat_map;

import java.util.Arrays;
import java.util.List;

public class HBO {
    public static void main(String[] args) {
        Video video = new Video("GOT1", "got1.com", VideoType.CLIP, 1);
        Video video1 = new Video("GOT2", "got2.com", VideoType.EPISODE, 2);
        Video video2 = new Video("GOT3", "got3.com", VideoType.PREVIEW, 3);
        Video video3 = new Video("GOT4", "got4.com", VideoType.PREVIEW, 4);
        Video video4 = new Video("GOT5", "got5.com", VideoType.CLIP, 5);
        Video video5 = new Video("GOT6", "got6.com", VideoType.EPISODE, 6);

        Episode episode = new Episode("got1", 1,
                Arrays.asList(video, video1));
        Episode episode1 = new Episode("got2", 2,
                Arrays.asList(video2, video3));
        Episode episode2 = new Episode("got3", 3,
                Arrays.asList(video4, video5));

        Season season = new Season("GOTS1", 1,
                Arrays.asList(episode, episode1));

        Season season1 = new Season("GOTS2", 2,
                Arrays.asList(episode2));

        List<Season> seasons = Arrays.asList(season, season1);

        //==========Part 1==========
        //1. Zwróć listę wszystkich episodów
        seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .forEach((Episode e) -> System.out.println(e));

        //2. Zwróć listę wszystkich filmów
        seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .flatMap((Episode e) -> e.videos.stream())
                .forEach((Video v) -> System.out.println(v));

        //3. Zwróć listę wszystkich nazw sezonów
        seasons.stream()
                .map((Season s) -> s.seasonName)
                .forEach((String n) -> System.out.println(n));

        //4. Zwróć listę wszystkich numerów sezonów
        seasons.stream()
                .map((Season s) -> s.seasonNumber)
                .forEach((Integer i) -> System.out.println(i));

        //5. Zwróć listę wszystkich nazw episodów
        seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .map((Episode e) -> e.episodeName)
                .forEach((String s) -> System.out.println(s));

        //6. Zwróć listę wszystkich numerów episodów
        seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .map((Episode e) -> e.episodeNumber)
                .forEach((Integer i) -> System.out.println(i));

        //7. Zwróć listę wszystkich nazw video
        seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .flatMap((Episode e) -> e.videos.stream())
                .map((Video v) -> v.title)
                .forEach((String s) -> System.out.println(s));

        //8. Zwróć listę wszystkich numerów video
        seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .flatMap((Episode e) -> e.videos.stream())
                .map((Video v) -> v.videoNumber)
                .forEach((Integer i) -> System.out.println(i));

        ////Druga część
        //1. Wyświetl tylko epiosdy z parzystych sezonów
        seasons.stream()
                .filter((Season s) -> s.seasonNumber % 2 == 0)
                .flatMap((Season s) -> s.episodes.stream())
                .forEach((Episode e) -> System.out.println(e));

        //2. Wyświetl tylko video z parzystych sezonów
        seasons.stream()
                .filter((Season s) -> s.seasonNumber % 2 == 0)
                .flatMap((Season s) -> s.episodes.stream())
                .flatMap((Episode e) -> e.videos.stream())
                .forEach((Video v) -> System.out.println(v));

        //3. Wyświetl tylko video z parzystych episody i sezonów
        seasons.stream()
                .filter((Season s) -> s.seasonNumber % 2 == 0)
                .flatMap((Season s) -> s.episodes.stream())
                .filter((Episode e) -> e.episodeNumber % 2 == 0)
                .flatMap((Episode e) -> e.videos.stream())
                .forEach((Video v) -> System.out.println(v));

        //4. Wyświetl tylko nieparzyste video z parzystych episodów i nieparzystych sezonów
        seasons.stream()
                .filter((Season s) -> s.seasonNumber % 2 == 1)
                .flatMap((Season s) -> s.episodes.stream())
                .filter((Episode e) -> e.episodeNumber % 2 == 0)
                .flatMap((Episode e) -> e.videos.stream())
                .filter((Video v) -> v.videoNumber % 2 == 1)
                .forEach((Video v) -> System.out.println(v));

        //5. Wyświetl tylko parzyste video z nieparzystych episodów i parzystych sezonów
        seasons.stream()
                .filter((Season s) -> s.seasonNumber % 2 == 0)
                .flatMap((Season s) -> s.episodes.stream())
                .filter((Episode e) -> e.episodeNumber % 2 == 1)
                .flatMap((Episode e) -> e.videos.stream())
                .filter((Video v) -> v.videoNumber % 2 == 0)
                .forEach((Video v) -> System.out.println(v));
    }


    enum VideoType {
        CLIP, PREVIEW, EPISODE
    }

    static class Video {
        public String title;
        public String url;
        public VideoType videoType;
        public Integer videoNumber;

        public Video(String title, String url, VideoType videoType, Integer videoNumber) {
            this.title = title;
            this.url = url;
            this.videoType = videoType;
            this.videoNumber = videoNumber;
        }

        @Override
        public String toString() {
            return "Video{" +
                    "title='" + title + '\'' +
                    ", url='" + url + '\'' +
                    ", videoType=" + videoType +
                    '}';
        }
    }

    static class Episode {
        public String episodeName;
        public int episodeNumber;
        List<Video> videos;

        public Episode(String episodeName, int episodeNumber, List<Video> videos) {
            this.episodeName = episodeName;
            this.episodeNumber = episodeNumber;
            this.videos = videos;
        }

        @Override
        public String toString() {
            return "Episode{" +
                    "episodeName='" + episodeName + '\'' +
                    ", episodeNumber=" + episodeNumber +
                    ", videos=" + videos +
                    '}';
        }
    }

    static class Season {
        public String seasonName;
        public int seasonNumber;
        List<Episode> episodes;

        public Season(String seasonName, int seasonNumber, List<Episode> episodes) {
            this.seasonName = seasonName;
            this.seasonNumber = seasonNumber;
            this.episodes = episodes;
        }

        @Override
        public String toString() {
            return "Season{" +
                    "seasonName='" + seasonName + '\'' +
                    ", seasonNumber=" + seasonNumber +
                    ", episodes=" + episodes +
                    '}';
        }
    }
}
