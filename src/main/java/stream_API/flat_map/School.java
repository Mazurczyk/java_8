package stream_API.flat_map;

import java.util.Arrays;
import java.util.List;

public class School {

    public static void main(String[] args) {
        Teacher teacher = new Teacher();
        Teacher teacher1 = new Teacher();
        List<Teacher> teachers = Arrays.asList(teacher, teacher1);

        //- znajdź tylko studentów nauczycieli uczących biologi
        teachers.stream()
                .filter((Teacher t) -> t.subject.equals("Biology"))
                .forEach((Teacher t) -> System.out.println(t));

        //- znajdź tylko studentów - kobiety (Imiona kończą się na a)
        teachers.stream()
                .flatMap((Teacher t) -> t.schoolClass.stream())
                .flatMap((SchoolClass s) -> s.students.stream())
                .filter((Student s) -> s.name.endsWith("a"))
                .forEach((Student s) -> System.out.println(s));

        //- znajdź tylko klasy gdzie liczba studentów jest większa od 3
        teachers.stream()
                .flatMap((Teacher t) -> t.schoolClass.stream())
                .filter((SchoolClass s) -> s.students.size() > 3)
                .forEach((SchoolClass s) -> System.out.println(s));

        //- znajdź klasy które mają id z serii 2000 czyli większe od 2000
        teachers.stream()
                .flatMap((Teacher t) -> t.schoolClass.stream())
                .filter((SchoolClass s) -> Integer.parseInt(s.id) > 2000)
                .forEach((SchoolClass s) -> System.out.println(s));

    }

    static class Teacher {
        String name;
        String lastName;
        String subject;
        List<SchoolClass> schoolClass;
    }

    static class SchoolClass {
        String name;
        String id;
        List<Student> students;
    }

    static class Student {
        String name;
        String lastName;
        int id;
    }
}
