package stream_API.map;

//Na potrzeby zadania skorzystaj z następującej listy:
//Arrays.asList(“12:34”, “07:07”);
//3. Napisz funkcję która dowolną datę w postaci string zamieni na postać LocalTime

import java.lang.reflect.Array;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task3 {
    public static void main(String[] args) {
        List<String> times = Arrays.asList("12:34", "07:07");
        System.out.println(toLocalTime(times));
    }

    public static List<LocalTime> toLocalTime(List<String> times) {
        return times.stream()
                .map((String time) -> {
                    String[] data = time.split(":");
                    return LocalTime.of(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
                })
                .collect(Collectors.toList());
    }
}
