package stream_API.map;

//        Na potrzeby zadania skorzystaj z następującej listy:
//        Arrays.asList(“PiOTr”, “joANNa”, “krzYsztof”);
//        1. Napisz funkcję, która wyświetli każdy element listy gdzie każdy jej element jest wcześniej ustawiany jako
//        ciąg znaków z małych liter, np PiOTr->PIOTR

import java.util.Arrays;
import java.util.List;

public class Task1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("PiOTr", "joANNa", "krzYsztof");
        printUppercasedNames(names);
    }

    public static void printUppercasedNames(List<String> names){
        names.stream()
                .map((String name) -> name.toUpperCase())
                .forEach((String name) -> System.out.println(name));
    }
}
