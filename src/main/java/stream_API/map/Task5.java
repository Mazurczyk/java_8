package stream_API.map;

//5. Napisz funkcję która zmapuje nam listę lini plików CSV na obiekty klasy Car
//Arrays.asList(“VW,50”, “SEAT,100”, “SKODA,150”)

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task5 {
    public static void main(String[] args) {
        List<String> cars = Arrays.asList("VW,50", "SEAT,100", "SKODA,150");
        System.out.println(toCars(cars));
    }

    public static List<Car> toCars(List<String> cars) {
        return cars.stream()
                .map((String car) -> car.split(","))
                .map((String[] data) -> new Car(data[0], Integer.parseInt(data[1])))
                .collect(Collectors.toList());
    }
}
