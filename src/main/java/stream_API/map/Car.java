package stream_API.map;

public class Car {
    String carName;
	int speed;

    public Car(String carName, int speed) {
        this.carName = carName;
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carName='" + carName + '\'' +
                ", speed=" + speed +
                '}';
    }
}
