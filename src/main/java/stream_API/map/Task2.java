package stream_API.map;

//Na potrzeby zadania skorzystaj z następującej listy:
//Arrays.asList(10, 200, 300);
//2. Napisz funkcję, która do każdego elementu listy doda wartość 10.

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(10, 200, 300);
        System.out.println(add10ForEach(integers));
    }

    public static List<Integer> add10ForEach(List<Integer> integers) {
        return integers.stream()
                .map((Integer i) -> i + 10)
                .collect(Collectors.toList());
    }
}
