package stream_API.map;

//4. Napisz funkcję mam która dla poniższej listy zwróci:
//class Car {
//	String carName;
//	int speed;
//}
//Arrays.asList(new Car(“VW”, 250), new Car(“Audi”, 300));
//wyświetli nam tylko prędkości danych samochodów

import java.util.Arrays;
import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<Car> cars = Arrays.asList(new Car("VW", 250), new Car("Audi", 300));
        showSpeeds(cars);
    }

    public static void showSpeeds(List<Car> cars) {
        cars.stream()
                .map((Car car) -> car.speed)
                .forEach((Integer speed) -> System.out.println(speed));
    }
}

