package stream_API.filter;

//4. Napisz funkcję, która dla poniższego przykładu
//List<Movie> movies = Arrays.asList(….)
//gdzie Movie to klasa
//class Movie {
//	String title;
//	String type;
//	long duration;//min
//	LocalDate releaseDate
//}
//zwróci listę filmów, gdzie
//- długość filmy będzie większa niż 20 min
//- rok wydania nie będzie starszy niż 2018
//- typ to horror

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task4 {
    public static void main(String[] args) {
        List<Movie> movies = Arrays.asList(new Movie("Rambo", "akcja", 120, LocalDate.of(1980, 12, 12)),
                new Movie("Krotki film", "horror", 15, LocalDate.of(2019, 12, 12)),
                new Movie("Dlugi horror", "horror", 120, LocalDate.of(2018, 12, 12)));
        System.out.println(getFilteredMovies(movies));
    }

    public static List<Movie> getFilteredMovies(List<Movie> movies) {
        return movies.stream()
                .filter((Movie m) -> m.type.equals("horror"))
                .filter((Movie m) -> m.duration > 20)
                .filter((Movie m) -> m.releaseDate.getYear() >= 2018)
                .collect(Collectors.toList());
    }
}
