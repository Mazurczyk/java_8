package stream_API.filter;

//2. Napisz funkcję, która dla poniższego przykładu:
//List<Rectangle> users = Arrays.asList(new Rectangle(4,4), new Rectangle(6,8));
//class Rectangle {
//    int a;
//    int b;
//    public Rectangle(int a, int b) {
//        this.a = a;
//        this.b = b;
//    }
//}
//Zwróci nam pola powierzchni prostokątków o polach większych niż 30

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task2 {
    public static void main(String[] args) {
        List<Rectangle> rectangles = Arrays.asList(new Rectangle(4, 4), new Rectangle(6, 8));
        System.out.println(getPolesMoreThan30(rectangles));
    }

    public static List<Integer> getPolesMoreThan30(List<Rectangle> rectangles) {
        return rectangles.stream()
                .map((Rectangle r) -> r.a * r.b)
                .filter((Integer i) -> i > 30)
                .collect(Collectors.toList());
    }
}
