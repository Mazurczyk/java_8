package stream_API.filter;

//3. Napisz funkcję, która dla poniższego przykładu:
//List<Rectangle> users = Arrays.asList(new Rectangle(4,4), new Rectangle(6,8));
//class Rectangle {
//    int a;
//    int b;
//    public Rectangle(int a, int b) {
//        this.a = a;
//        this.b = b;
//    }
//}
//Zwróci nam prostokąty, które mają taką samą długość boków.

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task3 {
    public static void main(String[] args) {
        List<Rectangle> rectangles = Arrays.asList(new Rectangle(4, 4), new Rectangle(6, 8));
        System.out.println(getSquares(rectangles));
    }

    public static List<Rectangle> getSquares(List<Rectangle> rectangles) {
        return rectangles.stream()
                .filter((Rectangle r) -> r.a == r.b)
                .collect(Collectors.toList());
    }
}
