package stream_API.filter;

//Na potrzeby zadania skorzystaj z następującej listy:
//Arrays.asList(12,13,16);
//1. Napisz funkcję która zwróci tylko elementy nieparzyste
//- Napisz funkcję która zwróci tylko elementy parzyste

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task1 {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(12, 13, 16);
        System.out.println(getEvens(integers));
    }

    public static List<Integer> getEvens(List<Integer> integers) {
        return integers.stream()
                .filter((Integer i) -> i % 2 == 0)
                .collect(Collectors.toList());
    }
}
