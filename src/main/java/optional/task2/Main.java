package optional.task2;

//Na podstawie listy użytkowników znajdź użytkownika o peselu:
//76434340
//​
//class User {
//	String name;
//	String lastName;
//	long id;
//}
//​
//napisz metodę która wyszuka nam użytkowników po id na podstawie poniżej listy:
//​
//List<User> users = Arrays.asList(new User(“Piotr”, “Brzozowski”, 9978934), new User(“Wacław”, “Kowalski”, 743644));
//​
//Jeśli użytkownik o danym peselu istnieje wyświetl go, jeśli nie zwróć wyjatek: NoSuchElementException

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class Main {
    public static void main(String[] args) {
        List<User> users = Arrays.asList(new User("Piotr", "Brzozowski", 9978934),
                new User("Wacław", "Kowalski", 743644));
        User user = getUserByID(users, 76434340L);
        System.out.println(user);
    }

    public static User getUserByID(List<User> users, Long ID) {
        return users.stream()
                .filter(u -> ID != null && ID == u.id)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No user found!"));
    }
}