package optional.task1;

//Zaprezentuj działanie obiekty Optional wrappującego klasę Movie:
//class Movie {
//	String title;
//	String description;
//	int yearOfRelease;
//}
//​
//zwróć domyślny obiekt jeśli Optional jest pusty
//jeśli Optional istnieje wyświetl film w formacie:
//tytuł->opis->data_wydania

import java.util.Optional;

public class Main {

    public static final Movie MOVIE_DEFAULT = new Movie("NOT_DEFINED", "NOT_DEFINED", 1900);

    public static void main(String[] args) {

        Movie movie = null;
        Movie movie1 = new Movie("adad","asdad",1990);
        Optional<Movie> movieOptional = Optional.ofNullable(movie);
        movieOptional.ifPresent(System.out::println);
        movieOptional.orElseGet(() -> {
            System.out.println(MOVIE_DEFAULT);
            return MOVIE_DEFAULT;
        });
    }
}
