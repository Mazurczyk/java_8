package optional.task1;

import java.util.function.Supplier;

public class Movie {

    String title;
    String description;
    int yearOfRelease;

    public Movie(String title, String description, int yearOfRelease) {
        this.title = title;
        this.description = description;
        this.yearOfRelease = yearOfRelease;
    }

    //tytuł->opis->data_wydania
    @Override
    public String toString() {
        return "title -> " + title + ", description ->" + description + ", yearOfRelease -> " + yearOfRelease;
    }
}
