package function_interfaces.predicate;

import function_interfaces.consumer.User;

import java.util.function.Predicate;

public class Task3 {
    public static void main(String[] args) {
        User user = new User("Rafał", "Mazurczyk", 17);
        Predicate<User> adultChecker = (User u) -> u.age >= 18;
        System.out.println(adultChecker.test(user));
    }
}
