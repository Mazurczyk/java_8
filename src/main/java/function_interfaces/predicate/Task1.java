package function_interfaces.predicate;

import java.util.function.Predicate;

public class Task1 {
    public static void main(String[] args) {
        Integer integer = 14;
        Predicate<Integer> evenChecker = (Integer i) -> i % 2 == 0;
        System.out.println(evenChecker.test(integer));
    }
}
