package function_interfaces.predicate;

import java.util.function.Predicate;

public class Task2 {
    public static void main(String[] args) {
//        Year is multiple of 400.
//        Year is multiple of 4 and not multiple of 100.
        Integer year = 2100;
        Predicate<Integer> leapYearChecker = (Integer y) -> {
            boolean isLeap = false;
            if (y % 400 == 0 || (y % 4 == 0 && y % 100 != 0)) isLeap = true;
            return isLeap;
        };
        System.out.println(leapYearChecker.test(year));
    }
}
