package function_interfaces.predicate;

import java.util.function.Predicate;

public class Task4 {
    //    Napisz funkcję, która powinna sprawdzać czy podany numer telefonu jest poprawny.
    //    Numer powinien się zaczynać od +48 i zawierać 12 znaków.
    public static void main(String[] args) {
        String phone = "+48505329628";
        Predicate<String> phoneValidator = (String p) -> p.length() == 12 && p.startsWith("+48");
        System.out.println(phoneValidator.test(phone));
    }
}
