package function_interfaces.consumer;

import java.util.function.Consumer;

public class Task3 {
    public static void main(String[] args) {
        User user = new User("Rafał", "Mazurczyk", 27);
        Consumer<User> userPresenter = (User u) -> {
            System.out.println("***************");
            System.out.println("Imię: " + u.name);
            System.out.println("Nazwisko: " + u.lastName);
            System.out.println("Wiek: " + u.age);
            System.out.println("***************");
        };
        userPresenter.accept(user);
    }
}
