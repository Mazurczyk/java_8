package function_interfaces.consumer;

import java.util.ArrayList;
import java.util.List;

class Users {
    List<User> users = new ArrayList<>();

    public void addUser(User user){
        users.add(user);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
