package function_interfaces.consumer;

import java.util.function.Consumer;

public class Task4 {
    public static void main(String[] args) {
        User user1 = new User("Rafał", "Mazurczyk", 27);
        User user2 = new User("Paweł", "Kowalski", 23);
        User user3 = new User("Michał", "Nowak", 21);
        Users users = new Users();
        users.addUser(user1);
        users.addUser(user2);
        users.addUser(user3);
        Consumer<Users> usersPresenter = (Users uList) -> {
            for (User u : uList.getUsers()) {
                System.out.println("###############");
                System.out.println("***************");
                System.out.println("Imię: " + u.name);
                System.out.println("Nazwisko: " + u.lastName);
                System.out.println("Wiek: " + u.age);
                System.out.println("***************");
                System.out.println("###############");
                System.out.println();
            }
        };
        usersPresenter.accept(users);
    }
}
