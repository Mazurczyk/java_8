package function_interfaces.consumer;

import java.util.function.Consumer;

public class Task2 {
    public static void main(String[] args) {
        User user = new User("Rafał", "Mazurczyk", 27);
        Consumer<User> userConsumer = (User u) -> {
            u.name = u.name.toUpperCase();
            u.lastName = u.lastName.toUpperCase();
        };
        userConsumer.accept(user);
        System.out.println(user.name);
        System.out.println(user.lastName);
    }
}
