package function_interfaces.consumer;

import java.util.function.Consumer;

public class Task1 {
    public static void main(String[] args) {

    String time = "08:20";
    Consumer<String> timePresenter = (String t) -> {
        String[] dataTime = t.split(":");
        System.out.println("Godzina: " + dataTime[0]);
        System.out.println("Minuty: " + dataTime[1]);
    };

    timePresenter.accept(time);

    }
}
